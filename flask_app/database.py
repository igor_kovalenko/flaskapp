# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from os.path import join


Base = declarative_base()


class Database(object):
    _engine = None
    _session = None
    _database_name = None
    _app = None

    @property
    def session(self):
        return self._session

    @property
    def engine(self):
        return self._engine

    def __init__(self, app):
        self._app = app
        self._database_name = app.config['DATABASE_FILE']
        self._engine = create_engine(
            'sqlite:///{0}'.format(join(app.instance_path, self._database_name)),
            convert_unicode=True,
            echo=app.config['SQLALCHEMY_ECHO']
        )
        self._session = scoped_session(
            sessionmaker(autocommit=False, autoflush=False, bind=self._engine))
        Base.query = self._session.query_property()

        app.db = self

    def __repr__(self):
        return '<Database {0}>'.format(self._database_name)
