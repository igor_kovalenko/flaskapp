from flask_testing import TestCase
import os
from importlib import import_module
from alembic.config import Config
from alembic import command


class BaseTestCase(TestCase):
    """ Base Tests """

    _app = None

    def create_app(self):
        self._app = import_module('flask_app').app
        return self._app

    def setUp(self):
        alembic_cfg = Config(os.path.join(self._app.instance_path, 'alembic.ini'))
        alembic_cfg.set_section_option(
            'alembic',
            'sqlalchemy.url',
            "sqlite:///{0}".format(os.path.join(self._app.instance_path, 'test.db'))
        )
        command.upgrade(alembic_cfg, "head")

    def tearDown(self):
        os.remove(os.path.join(self._app.instance_path, self._app.config['DATABASE_FILE']))
