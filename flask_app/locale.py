# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from flask_babelex import Babel
from flask import g, request


def init_locale(app):
    babel = Babel(app)

    @babel.localeselector
    def get_locale():
        user = getattr(g, 'user', None)
        if user is not None:
            return user.locale

        return request.accept_languages.best_match(
            [l for l in app.config['LANGUAGES'].keys()], app.config['LANGUAGE_CODE'])

    @babel.timezoneselector
    def get_timezone():
        user = getattr(g, 'user', None)
        if user is not None:
            return user.timezone
        return app.config['TIME_ZONE']
