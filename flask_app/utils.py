# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import string
import random


def generate_random_secret_key():
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for __ in range(24))
