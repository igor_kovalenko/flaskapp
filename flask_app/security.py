# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from flask_security import Security, SQLAlchemySessionUserDatastore
from .models import User, Role


def init_security(app):
    app.user_datastore = SQLAlchemySessionUserDatastore(app.db.session, User, Role)
    app.security = Security(app, app.user_datastore)

    return app.security, app.user_datastore
