# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from sqlalchemy import Boolean, DateTime, Column, Integer, String, ForeignKey
from .database import Base
from flask_security import UserMixin, RoleMixin
from sqlalchemy.orm import relationship, backref


class RolesUsers(Base):
    __tablename__ = 'roles_users'
    id = Column(Integer(), primary_key=True)
    user_id = Column('user_id', Integer(), ForeignKey('user.id'))
    role_id = Column('role_id', Integer(), ForeignKey('role.id'))


class Role(Base, RoleMixin):
    __tablename__ = 'role'
    id = Column(Integer(), primary_key=True)
    name = Column(String(80), unique=True)
    description = Column(String(255))

    def __init__(self, name=None, description=None):
        self.name = name
        self.description = description

    def __repr__(self):
        return '<Role {0}>'.format(self.name)


class User(Base, UserMixin):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    email = Column(String(255), unique=True)
    username = Column(String(255))
    password = Column(String(255))
    last_login_at = Column(DateTime())
    current_login_at = Column(DateTime())
    last_login_ip = Column(String(100))
    current_login_ip = Column(String(100))
    login_count = Column(Integer)
    active = Column(Boolean())
    confirmed_at = Column(DateTime())
    roles = relationship('Role', secondary='roles_users',
                         backref=backref('users', lazy='dynamic'))

    def __init__(self, email, username=None, password=None, last_login_at=None,
                 current_login_at=None, last_login_ip=None, current_login_ip=None,
                 login_count=0, active=True, confirmed_at=None, roles=None):
        self.email = email
        self.username = username
        self.password = password
        self.last_login_at = last_login_at
        self.last_login_ip = last_login_ip
        self.current_login_at = current_login_at
        self.current_login_ip = current_login_ip
        self.login_count = login_count
        self.active = active
        self.confirmed_at = confirmed_at
        self.roles = roles

    def __repr__(self):
        return '<User {0}>'.format(self.username)
