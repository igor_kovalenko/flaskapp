# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from flask import redirect, request, url_for, abort
from flask_admin.contrib.sqla import ModelView
from flask_admin import Admin
from flask_admin import helpers as admin_helpers
from flask_security import current_user
from .models import Role, User


class AdminModelView(ModelView):

    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        if current_user.has_role('superuser'):
            return True

        return False

    def _handle_view(self, name, **kwargs):
        """
        Override builtin _handle_view in order to redirect users when a view is not accessible.
        """
        if not self.is_accessible():
            if current_user.is_authenticated:
                # permission denied
                abort(403)
            else:
                # login
                return redirect(url_for('security.login', next=request.url))


def init_admin(app):
    admin = Admin(
        app,
        'Example: Auth',
        base_template='my_master.html',
        template_mode='bootstrap3',
    )

    admin.add_view(AdminModelView(Role, app.db.session))
    admin.add_view(AdminModelView(User, app.db.session))

    @app.security.context_processor
    def security_context_processor():
        return dict(
            admin_base_template=admin.base_template,
            admin_view=admin.index_view,
            h=admin_helpers,
            get_url=url_for
        )

    return admin
