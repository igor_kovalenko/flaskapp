# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from .database import Database
from .admin import init_admin, AdminModelView
from .models import User, Role
from .security import init_security
from .locale import init_locale
from flask import Flask
import inspect
import click
from werkzeug.utils import import_string
import os
from os.path import join, dirname
from importlib import import_module
from flask_babelex import lazy_gettext as _
from flask_security.utils import hash_password


_dummy_env = {'wsgi.url_scheme': "", 'SERVER_PORT': "", 'SERVER_NAME': "", 'REQUEST_METHOD': ""}


def register_blueprints(app):
    """
    Register all blueprint modules
    """
    for name in app.config['BLUEPRINTS']:
        try:
            import_string(name + '.admin')
        except ImportError:
            pass
        mod = import_string(name + '.views')
        if hasattr(mod, 'bp'):
            app.register_blueprint(mod.bp)
    return None


def register_cli(app):
    @app.cli.command('create_superuser')
    @click.option('--username', prompt='Superuser username', help='Superuser username')
    @click.option('--email', prompt='Superuser email', help='Superuser email')
    @click.option('--password', prompt=True, hide_input=True, confirmation_prompt=True)
    def create_superuser_command(username, email, password):
        """Creates superuser."""
        superuser_role = app.user_datastore.find_or_create_role('superuser')

        superuser = app.user_datastore.create_user(username=username, email=email,
                                                   password=hash_password(password))
        app.user_datastore.add_role_to_user(superuser, superuser_role)

        app.db.session.commit()

        with app.request_context(_dummy_env):
            click.echo(_('Superuser successful created.'))


def register_teardown(app):
    @app.teardown_appcontext
    def shutdown_session(exception=None):
        app.db.session.remove()


_instance_path = dirname(inspect.getabsfile(import_module((os.environ.get("FLASK_SETTINGS")))))

app = Flask(__name__, instance_path=_instance_path, static_folder=join(_instance_path, 'static'))

# Инициализируем конфигурацию приложения
app.config.from_pyfile(join(dirname(__name__), 'default_settings.py'), silent=True)
app.config.from_object(os.environ.get("FLASK_SETTINGS"))

if bool(os.environ.get("TESTING", False)):
    app.config['DATABASE_FILE'] = 'test.db'

# Инициалируем ОРМ
db = Database(app)

# Инициализируем слой управления ролями и пользователями
security, user_datastore = init_security(app)

# Инициализируем поддержку I18n и L10n
# babel = Babel(app)
init_locale(app)

# Регистириуем обработчики приложения
register_cli(app)
register_teardown(app)

# Инициализируем административный интрефейс
admin = init_admin(app)

# Регистриуем черновики
register_blueprints(app)
