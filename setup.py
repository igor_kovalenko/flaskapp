# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='FlaskApp',
    version='0.0.1',
    description='Flask+SqlAlchemy+FlaskAdmin+FlaskSecurity',
    long_description=open(join(dirname(__file__), 'README')).read(),
    author='Igor S. Kovalenko',
    author_email='kovalenko@sb-soft.biz',
    packages=find_packages(),
    platforms='any',
    zip_safe=False,
    include_package_data=True,
    install_requires=[
        'Flask==0.12.2',
        'Flask-Admin==1.5.0',
        'Flask-Login==0.4.0',
        'Flask-Security==3.0.0',
        'flask-restplus==0.10.1',
        'Flask-BabelEx==0.9.3',
        'Flask-Testing==0.6.2',
        'alembic==0.9.6',
    ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Framework :: Flask :: 0.12.2',
    ],
)
